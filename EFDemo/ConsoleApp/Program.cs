﻿using DataModel;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new NullDatabaseInitializer<StudentContext>());
            //InsertStudent();
            //InsertMultipleStudents();
            //QueryStudent();
            //QueryAndUpdateStudent();
            //UsingStoredProcedure();

            //DeleteStudent();
            //ComplexInsert();
            //QueryingComplexObjects
            AddedStoredProcedure();
            Console.ReadKey();
        }

        static void AddedStoredProcedure()
        {
            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                context.Database.ExecuteSqlCommand(@"CREATE PROCEDURE GetOlaf as select * from Students where Name = 'Olaf'");
            }
        }

        static void QueryingComplexObjects()
        {
            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;
                
                //var student = context.Students.FirstOrDefault(s => s.Name == "Olaf");

                //eager loading - ładowanie zachłanne
                //var student = context.Students.Include(s => s.Grades).FirstOrDefault(s => s.Name == "Olaf");
                
                var student = context.Students.FirstOrDefault(s => s.Name == "Olaf");

                //explicit loading - ładowanie jawne
                //context.Entry(student).Collection(s => s.Grades).Load();

                Console.WriteLine(student.Grades.Count());
            }
        }

        static void ComplexInsert()
        {

            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                var student = new Student()
                {
                    Name = "Olaf",
                    ClassId = 1,
                    Passed = false
                };

                var mathGrade = new StudentGrade
                {
                    Subject = Subject.Math,
                    Grade = 4,
                    Name = "Dobry"
                };

                var mathGrade2 = new StudentGrade
                {
                    Subject = Subject.Math,
                    Grade = 5,
                    Name = "Bardzo dobry"
                };

                student.Grades.AddRange(new List<StudentGrade> { mathGrade, mathGrade2 });

                context.Students.Add(student);

                context.SaveChanges();

            }
        }

        static void DeleteStudent()
        {

            Student student;

            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                student = context.Students.Where(s => s.Name == "Mateusz").FirstOrDefault();
                
            }

            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                context.Students.Attach(student);

                //1 opcja
                //context.Students.Remove(student);

                //2 opcja
                context.Entry(student).State = EntityState.Deleted;

                context.SaveChanges();
            }
        }

        static void UsingStoredProcedure()
        {
            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                var students = context.Students.SqlQuery("exec GetStudentsNamedMateusz");

                foreach (var student in students)
                {
                    Console.WriteLine(student.Name);
                }
                
            }
        }

        static void QueryAndUpdateStudent()
        {

            Student student;

            //połączona aplikacja
            //using (var context = new StudentContext())
            //{
            //    context.Database.Log = Console.WriteLine;

            //    student = context.Students.Where(s => s.Name == "Mateusz").FirstOrDefault();

            //    student.Name = "Marcin";

            //    context.SaveChanges();
            //}

            //rozłączona aplikacja
            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                student = context.Students.Where(s => s.Name == "Mateusz").FirstOrDefault();

            }

            student.Name = "Mariusz";

            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                context.Students.Attach(student);
                context.Entry(student).State = EntityState.Modified;

                context.Database.Log = Console.WriteLine;

                context.SaveChanges();
            }

        }

            static void QueryStudent()
        {
            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;

                //var query = context.Students.ToList();

                var query = context.Students.Where(s => s.Name == "Łukasz");

                foreach (var student in query)
                {
                    Console.WriteLine(student.Name);
                }
            }
        }

        static void InsertStudent()
        {
            var student = new Student
            {
                Name = "Mateusz",
                Passed = true,
                ClassId = 1                
            };

            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;
                context.Students.Add(student);
                context.SaveChanges();
            }
        }
        static void InsertMultipleStudents()
        {
            var student = new Student
            {
                Name = "Mateusz",
                Passed = true,
                ClassId = 1
            };

            var student2 = new Student
            {
                Name = "Łukasz",
                Passed = false,
                ClassId = 1
            };

            using (var context = new StudentContext())
            {
                context.Database.Log = Console.WriteLine;
                context.Students.AddRange(new List<Student> { student, student2 });
                context.SaveChanges();
            }
        }
    }
}
