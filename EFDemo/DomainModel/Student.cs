﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DomainModel
{
    public class Student
    {
        public Student()
        {
            Grades = new List<StudentGrade>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Passed { get; set; }

        public int ClassId { get; set; }
        public Class Class { get; set; }

        public List<StudentGrade> Grades { get; set; }
    }

    public class StudentGrade
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Grade { get; set; }

        [Required]
        public Subject Subject { get; set; }
        
        public Student Student { get; set; }
    }

    public class Class
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Student> Students { get; set; }
    }

}
