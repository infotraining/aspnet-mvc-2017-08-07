﻿namespace DomainModel
{
    public enum Subject
    {
        Math = 1,
        Programming = 2,
        Biology = 3
    }
}
