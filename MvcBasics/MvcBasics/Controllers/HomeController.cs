﻿using MvcBasics.Models;
using System.Web.Mvc;

namespace MvcBasics.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Message = "Hello World!";

            ViewBag.NewMessage = "Nowa wiadomość";

            return View();
        }

        [HttpGet]
        public ActionResult Enroll()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Enroll(Participant participant)
        {
            if (!ModelState.IsValid)
                return View(participant);

            //persystencja albo wysłanie mailem

            return View("Confirmation", participant);
        }

        public ActionResult ParticipantList()
        {
            Participant[] participants = {
                new Participant() {Name = "Łukasz", Email = "lukasz.zawadzki@infotraining.pl"},
                new Participant() {Name = "Mateusz", Email = "matuesz@arvato.pl"},
                new Participant() {Name = "Krystian", Email = "krystian@infotraining.pl"}
            };

            return View(participants);
        }
    }
}