﻿using System.ComponentModel.DataAnnotations;

namespace MvcBasics.Models
{
    public class Participant
    {
        [Required]
        public string Name { get; set; }
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Podaj prawidłowy email")]
        public string Email { get; set; }
        [Phone]
        [Required(ErrorMessage = "Podaj numer telefonu")]
        public string Phone { get; set; }
        [Required]
        public bool? Confirmed { get; set; }
    }
}