﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewsAndHelperMethods.Models
{
    public class PersonMetaData
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        [DisplayName("Data urodzenia")]
        [DataType(DataType.Date)]
        public DateTime DoB { get; set; }
    }
}