﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewsAndHelperMethods.Models;

namespace ViewsAndHelperMethods.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreatePerson()
        {
            return View(new Person() { DoB = new DateTime(1980/1/1) });
        }

        [HttpPost]
        public ActionResult CreatePerson(Person person)
        {
            return View("DisplayPerson", person);
        }

        // GET: Home
        public ActionResult Index2()
        {
            ViewBag.Students = new string[] { "Mateusz", "Krystian", "Łukasz" };
            ViewBag.Cities = new string[] { "Kraków", "Poznań", "Oslo" };

            return View();
        }

        [ChildActionOnly]
        public ActionResult Time()
        {
            return PartialView("_Time", DateTime.Now);
        }
    }
}