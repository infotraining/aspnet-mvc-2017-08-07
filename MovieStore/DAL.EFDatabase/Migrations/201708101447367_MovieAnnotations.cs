namespace DAL.EFDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MovieAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Movies", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "ShortDescription", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "Cover", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "CoverThumbnail", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Movies", "CoverThumbnail", c => c.String());
            AlterColumn("dbo.Movies", "Cover", c => c.String());
            AlterColumn("dbo.Movies", "Description", c => c.String());
            AlterColumn("dbo.Movies", "ShortDescription", c => c.String());
            AlterColumn("dbo.Movies", "Title", c => c.String());
        }
    }
}
