﻿using DomainModel;
using DomainModel.Repositories;
using System.Linq;

namespace DAL.EFDatabase.Repositories
{
    public class MoviesRepository : GenericRepository<Movie>, IMoviesRepository
    {
        public MoviesRepository(MovieStoreContext context) : base (context) { }
    }
}
