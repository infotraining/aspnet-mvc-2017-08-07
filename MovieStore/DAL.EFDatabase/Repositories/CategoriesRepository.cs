﻿using DomainModel;
using DomainModel.Repositories;

namespace DAL.EFDatabase.Repositories
{
    public class CategoriesRepository : GenericRepository<Category>, ICategoriesRepository
    {
        public CategoriesRepository(MovieStoreContext dbContext) : base(dbContext)
        {
        }
    }
}
