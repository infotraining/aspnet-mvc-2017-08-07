﻿using DomainModel;
using DomainModel.Repositories;

namespace DAL.EFDatabase.Repositories
{
    public class ReviewsRepository : GenericRepository<Review>, IReviewsRepository
    {
        public ReviewsRepository(MovieStoreContext dbContext) : base(dbContext)
        {
        }
    }
}
