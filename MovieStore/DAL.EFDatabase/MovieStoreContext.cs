﻿using DomainModel;
using System.Data.Entity;

namespace DAL.EFDatabase
{
    public class MovieStoreContext : DbContext
    {
        public MovieStoreContext() : base("DefaultConnection") { }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                .HasRequired(m => m.Category)
                .WithMany(c => c.Movies)
                .HasForeignKey(m => m.CategoryId);

            modelBuilder.Entity<Review>()
                .HasRequired(r => r.Movie)
                .WithMany(m => m.Reviews)
                .HasForeignKey(r => r.MovieId);
        }
    }
}
