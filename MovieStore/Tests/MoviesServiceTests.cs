﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using DomainModel.Repositories;
using DomainModel;
using System.Collections.Generic;
using System.Linq;
using Services.Infrastructure;
using Services.ViewModels;
using Services.Implementations;

namespace Tests
{
    [TestClass]
    public class MoviesServiceTests
    {
        [TestMethod]
        public void CanPaginate()
        {
            //arrange
            Mock<IMoviesRepository> mqMoviesRepository = new Mock<IMoviesRepository>();

            var movies = new List<Movie>
            {
                new Movie { MovieId = 1, Title = "Movie #1" },
                new Movie { MovieId = 3, Title = "Movie #2" },
                new Movie { MovieId = 6, Title = "Movie #3" },
                new Movie { MovieId = 9, Title = "Movie #4" },
                new Movie { MovieId = 10, Title = "Movie #5" }
            };

            mqMoviesRepository.Setup(m => m.GetAll()).Returns(movies.AsQueryable());

            var pagingHelper = new PagingHelper
            {
                CurrentPage = 2,
                ItemsPerPage = 3
            };

            var moviesService = new MoviesService(mqMoviesRepository.Object);

            //act
            MoviesListViewModel result = moviesService.GetMovies(pagingHelper);

            //assert

            var resultArray = result.Movies.ToArray();

            Assert.IsTrue(resultArray.Length == 2);
            Assert.AreEqual(resultArray[0].MovieId, 9);
            Assert.AreEqual(resultArray[1].MovieId, 10);
        }
    }
}
