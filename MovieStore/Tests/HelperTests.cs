﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Helpers;

namespace Tests
{
    [TestClass]
    public class PageLinksHelperTests
    {
        [TestMethod]
        public void CanGeneratePageLinks()
        {
            //arrange
            HtmlHelper html = null;

            PagingHelper helper = new PagingHelper
            {
                CurrentPage = 1,
                ItemsPerPage = 4,
                TotalItems = 7
            };

            Func<int, string> urlPage = i => "Strona " + i;

            //act
            MvcHtmlString result = html.PageLinks(helper, urlPage);

            //assert
            Assert.AreEqual(@"<a class=""btn btn-default selected btn-primary"" href=""Strona 1"">1</a>" + @"<a class=""btn btn-default"" href=""Strona 2"">2</a>", 
                result.ToString());
        }
    }
}
