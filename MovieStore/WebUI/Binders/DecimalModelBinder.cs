﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace WebUI.Binders
{
    public class DecimalModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            object result = null;

            string modelName = bindingContext.ModelName;
            string attemptedValue = bindingContext.ValueProvider.GetValue(modelName).AttemptedValue;

            string wantedSeparator = NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator;
            string alternateSeparator = wantedSeparator == "," ? "." : ",";

            if (attemptedValue.IndexOf(wantedSeparator) == -1 
                && attemptedValue.IndexOf(alternateSeparator) != -1)
            {
                attemptedValue = attemptedValue.Replace(alternateSeparator, wantedSeparator);
            }

            try
            {
                if (bindingContext.ModelMetadata.IsNullableValueType && string.IsNullOrEmpty(attemptedValue))
                    return null;

                result = decimal.Parse(attemptedValue, NumberStyles.Any);
            }
            catch (FormatException ex)
            {
                bindingContext.ModelState.AddModelError(modelName, ex.Message);
            }

            return result;
        }
    }
}