﻿using DomainModel;
using Services.Interfaces;
using Services.Messaging.Admin;
using System.Web.Mvc;

namespace WebUI.Controllers.Admin
{
    [Authorize]
    public class MoviesAdminController : Controller
    {
        private readonly IMoviesAdminService _service;

        public MoviesAdminController(IMoviesAdminService service)
        {
            _service = service;
        }

        // GET: MoviesAdmin
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Movie model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var request = new GetAddMovieRequest { Movie = model };

            var response = _service.AddMovie(request);

            if (!response.IsSuccess)
                return View(model);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Edit(int movieId)
        {
            var request = new GetMovieRequest { MovieId = movieId };

            var response = _service.GetMovie(request);

            if (!response.IsSuccess)
                return RedirectToAction("Index", "Home");

            return View(response.Movie);
        }

        [HttpPost]
        public ActionResult Edit(Movie movie)
        {
            if (!ModelState.IsValid)
                return View(movie);

            var request = new GetEditMovieRequest { Movie = movie };

            var response = _service.EditMovie(request);

            if (!response.IsSuccess)
                return View(movie);

            return RedirectToAction("Details", "Home", new { movieId = movie.MovieId });
        }

        [HttpPost]
        public ActionResult Delete(int movieId)
        {
            var request = new GetDeleteMovieRequest { MovieId = movieId };

            var response = _service.DeleteMovie(request);

            if (!response.IsSuccess)
                return RedirectToAction("Details", "Home", new { movieId = movieId });

            return RedirectToAction("Index", "Home");
        }
    }
}