﻿using Services.Interfaces;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICategoriesService _service;

        public CategoriesController(ICategoriesService service)
        {
            _service = service;
        }

        [ChildActionOnly]
        public ActionResult CategoryList()
        {
            var categories = _service.GetCategories();

            return PartialView(categories);
        }
    }
}