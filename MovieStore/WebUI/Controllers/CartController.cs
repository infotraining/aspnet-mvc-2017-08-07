﻿using DomainModel;
using Services.Interfaces;
using Services.Messaging.Admin;
using Services.ViewModels;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class CartController : Controller
    {
        private readonly IMoviesService _moviesService;
        private readonly ICartService _cartService;

        public CartController(IMoviesService moviesService, ICartService cartService)
        {
            _moviesService = moviesService;
            _cartService = cartService;
        }

        public Cart GetCartFromSession()
        {
            Cart cart = (Cart)Session["Cart"];

            if (cart == null)
            {
                cart = new Cart();
                Session["cart"] = cart;
            }

            return cart;
        }

        public ActionResult Index()
        {
            Cart cart = GetCartFromSession();

            CartViewModel model = _cartService.CreateCart(cart);

            return View(model);
        }

        public ActionResult CartSummary()
        {
            Cart cart = GetCartFromSession();

            CartSummary summary = cart.CartSummary;

            return PartialView("_CartSummary", summary);
        }

        public ActionResult AddToCart(int movieId)
        {
            var movie = _moviesService.GetMovie(movieId);

            var cart = GetCartFromSession();

            cart.AddItem(movie);

            var summary = cart.CartSummary;

            return PartialView("_CartSummary", summary);
        }

        public JsonResult RemoveFromCart(int movieId)
        {
            Movie movie = _moviesService.GetMovie(movieId);

            Cart cart = GetCartFromSession();

            cart.RemoveItem(movie);

            CartViewModel model = _cartService.CreateCart(cart);

            return Json(new
            {
                DeletedMovieId = movieId,
                TotalQuantity = model.TotalItems,
                TotalPrice = model.TotalPrice
            });
        }
    }
}
