﻿using DAL;
using DAL.EFDatabase;
using DAL.EFDatabase.Repositories;
using DomainModel;
using DomainModel.Repositories;
using Services.Infrastructure;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMoviesService _moviesService;

        public HomeController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        public ActionResult Index(int categoryId = 0, int currentPage = 1, int itemsPerPage = 5)
        {
            var pagingHelper = new PagingHelper
            {
                CategoryId = categoryId,
                CurrentPage = currentPage,
                ItemsPerPage = itemsPerPage
            };

            MoviesListViewModel model = _moviesService.GetMovies(pagingHelper);
            
            return View(model);
        }

        public ActionResult Details(int movieId)
        {
            Movie movie = _moviesService.GetMovie(movieId);

            return View(movie);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}