﻿using Services.Interfaces;
using Services.Messaging;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class ReviewsController : Controller
    {
        private readonly IReviewsService _reviewsService;

        public ReviewsController(IReviewsService reviewsService)
        {
            _reviewsService = reviewsService;
        }

        public ActionResult Index()
        {
            var reviews = _reviewsService.GetReviews();

            return View(reviews);
        }

        public ActionResult AddReview(int movieId, string movieTitle)
        {
            var model = new AddReviewViewModel
            {
                MovieId = movieId,
                MovieTitle = movieTitle
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult AddReview(AddReviewViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var request = new GetAddReviewRequest { Review = model };

            var response = _reviewsService.AddReview(request);

            if (!response.IsSuccess)
                return View(model);

            return RedirectToAction("Details", "Home", new { movieId = model.MovieId });
        }
    }
}