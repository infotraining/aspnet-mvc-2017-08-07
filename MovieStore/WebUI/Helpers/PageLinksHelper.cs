﻿using Services.Infrastructure;
using System;
using System.Text;
using System.Web.Mvc;

namespace WebUI.Helpers
{
    public static class PageLinksHelper
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingHelper pagingHelper, Func<int, string> urlPage)
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 1; i <= pagingHelper.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");

                tag.MergeAttribute("href", urlPage(i));

                tag.InnerHtml = i.ToString();

                if (i == pagingHelper.CurrentPage)
                {
                    tag.AddCssClass("btn-primary");
                    tag.AddCssClass("selected");
                }

                tag.AddCssClass("btn btn-default");

                builder.Append(tag.ToString());
            }

            return MvcHtmlString.Create(builder.ToString());
        }
    }
}