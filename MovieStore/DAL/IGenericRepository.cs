﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace DAL
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity GetById(int id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties);
        void Insert(TEntity entity);
        void Delete(object id);
        void Update(TEntity entity);
        void SaveChanges();
    }
}
