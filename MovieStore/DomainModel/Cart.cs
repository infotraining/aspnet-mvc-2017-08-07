﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public class Cart
    {
        private List<CartLine> _lines = new List<CartLine>();

        public IEnumerable<CartLine> Lines
        {
            get
            {
                return _lines;
            }
        }

        public void AddItem(Movie movie, int quantity = 1)
        {
            CartLine line = _lines.FirstOrDefault(l => l.Movie.MovieId == movie.MovieId);

            if (line == null)
            {
                _lines.Add(new CartLine { Movie = movie, Quantity = quantity });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveItem(Movie movie)
        {
            _lines.RemoveAll(c => c.Movie.MovieId == movie.MovieId);
        }

        public void ClearCart()
        {
            _lines.Clear();
        }

        public decimal TotalValue
        {
            get
            {
                return _lines.Sum(c => c.Movie.Price * c.Quantity);
            }
        }

        public CartSummary CartSummary
        {
            get
            {
                return new CartSummary
                {
                    TotalPrice = this.TotalValue,
                    TotalItems = _lines.Sum(c => c.Quantity)
                };
            }
        }
    }

    public class CartLine
    {
        public Movie Movie { get; set; }
        public int Quantity { get; set; }
    }

    public class CartSummary
    {
        public int TotalItems { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
