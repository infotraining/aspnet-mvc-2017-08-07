﻿using DAL;

namespace DomainModel.Repositories
{
    public interface IMoviesRepository : IGenericRepository<Movie>
    {
    }
}