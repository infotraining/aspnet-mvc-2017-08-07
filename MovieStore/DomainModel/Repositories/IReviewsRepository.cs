﻿using DAL;

namespace DomainModel.Repositories
{
    public interface IReviewsRepository : IGenericRepository<Review>
    {
    }
}
