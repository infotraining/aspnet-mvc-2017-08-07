﻿using DAL;

namespace DomainModel.Repositories
{
    public interface ICategoriesRepository : IGenericRepository<Category>
    {
    }
}
