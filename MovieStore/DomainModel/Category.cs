﻿using System.Collections.Generic;

namespace DomainModel
{
    public class Category
    {
        public Category()
        {
            Movies = new List<Movie>();
        }

        public int CategoryId { get; set; }
        public string Name { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}