﻿using System;

namespace DomainModel
{
    public class Review
    {
        public int ReviewId { get; set; }
        public string Author { get; set; }
        public DateTime DateAdded { get; set; }
        public int Rating { get; set; }
        public string Content { get; set; }

        public int MovieId { get; set; }
        public virtual Movie Movie { get; set; }
    }
}