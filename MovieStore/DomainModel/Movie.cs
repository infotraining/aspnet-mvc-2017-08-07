﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public class Movie
    {
        public Movie()
        {
            Reviews = new List<Review>();
        }
        
        public int MovieId { get; set; }
        [Required]
        [DisplayName("Tytuł")]
        public string Title { get; set; }
        [Required]
        [DisplayName("Krótki opis")]
        public string ShortDescription { get; set; }
        [Required]
        [DisplayName("Opis")]
        public string Description { get; set; }
        [Required]
        [DisplayName("Okładka")]
        public string Cover { get; set; }
        [Required]
        [DisplayName("Miniatura")]
        public string CoverThumbnail { get; set; }
        [Required]
        [DisplayName("Cena")]
        //[DataType(DataType.Currency)]
        public decimal Price { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }
    }
}
