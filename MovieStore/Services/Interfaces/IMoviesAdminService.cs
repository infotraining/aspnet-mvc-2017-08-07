﻿using Services.Messaging.Admin;

namespace Services.Interfaces
{
    public interface IMoviesAdminService
    {
        GetAddMovieResponse AddMovie(GetAddMovieRequest request);
        GetDeleteMovieResponse DeleteMovie(GetDeleteMovieRequest request);
        GetEditMovieResponse EditMovie(GetEditMovieRequest request);
        GetMovieResponse GetMovie(GetMovieRequest request);
    }
}
