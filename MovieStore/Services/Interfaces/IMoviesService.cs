﻿using DomainModel;
using Services.Infrastructure;
using Services.ViewModels;

namespace Services.Interfaces
{
    public interface IMoviesService
    {
        MoviesListViewModel GetMovies(PagingHelper pagingHelper);
        Movie GetMovie(int movieId);
    }
}
