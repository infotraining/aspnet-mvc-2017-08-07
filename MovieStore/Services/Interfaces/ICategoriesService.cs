﻿using DomainModel;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface ICategoriesService
    {
        IEnumerable<Category> GetCategories();
        Category GetCategory(int id);
    }
}
