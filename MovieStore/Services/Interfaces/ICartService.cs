﻿using DomainModel;
using Services.ViewModels;

namespace Services.Interfaces
{
    public interface ICartService
    {
        CartViewModel CreateCart(Cart cart);
    }
}
