﻿using Services.Messaging;
using Services.ViewModels;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IReviewsService
    {
        IEnumerable<ReviewViewModel> GetReviews();
        GetAddReviewResponse AddReview(GetAddReviewRequest request);
    }
}
