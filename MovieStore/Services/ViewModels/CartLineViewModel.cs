﻿namespace Services.ViewModels
{
    public class CartLineViewModel
    {
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public int Quantity { get; set; }
        public string MoviePrice { get; set; }
        public string Value { get; set; }
    }
}
