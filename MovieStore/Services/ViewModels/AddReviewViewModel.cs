﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ViewModels
{
    public class AddReviewViewModel : ReviewViewModel
    {
        public AddReviewViewModel()
        {
            this.DateAdded = DateTime.Now;

            RatingList = _options;
        }

        public IDictionary<int, string> RatingList { get; private set; }

        private static SortedDictionary<int, string> _options = new SortedDictionary<int, string>()
        {
            { 1, "Tragiczny" },
            { 2, "Słaby" },
            { 3, "Przeciętny" },
            { 4, "Dobry" },
            { 5, "Bardzo dobry" },
            { 6, "Arcydzieło" }
        };
    }
}
