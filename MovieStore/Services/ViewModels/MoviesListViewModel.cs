﻿using DomainModel;
using Services.Infrastructure;
using System.Collections.Generic;

namespace Services.ViewModels
{
    public class MoviesListViewModel
    {
        public IEnumerable<Movie> Movies { get; set; }
        public PagingHelper PagingHelper { get; set; }
    }
}
