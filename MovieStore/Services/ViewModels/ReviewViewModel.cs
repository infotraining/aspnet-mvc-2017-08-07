﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Services.ViewModels
{
    public class ReviewViewModel
    {
        [DisplayName("Autor")]
        [Required]
        public string Author { get; set; }
        [DisplayName("Data dodania")]
        [DataType(DataType.Date)]
        public DateTime DateAdded { get; set; }
        [DisplayName("Ocena")]
        public int Rating { get; set; }
        [DisplayName("Komentarz")]
        [Required]
        public string Content { get; set; }

        //public Movie Movie { get; set; }

        public int MovieId { get; set; }
        [DisplayName("Film")]
        public string MovieTitle { get; set; }
    }
}
