﻿using System.Collections.Generic;

namespace Services.ViewModels
{
    public class CartViewModel
    {
        public IEnumerable<CartLineViewModel> Lines { get; set; }
        public string TotalPrice { get; set; }
        public int TotalItems { get; set; }
    }
}
