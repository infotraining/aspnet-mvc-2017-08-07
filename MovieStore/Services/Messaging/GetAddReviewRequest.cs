﻿using Services.ViewModels;

namespace Services.Messaging
{
    public class GetAddReviewRequest
    {
        public AddReviewViewModel Review { get; set; }
    }
}
