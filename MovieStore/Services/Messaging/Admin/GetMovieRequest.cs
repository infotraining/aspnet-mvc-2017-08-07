﻿namespace Services.Messaging.Admin
{
    public class GetMovieRequest
    {
        public int MovieId { get; set; }
    }
}
