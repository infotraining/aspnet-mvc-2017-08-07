﻿using DomainModel;

namespace Services.Messaging.Admin
{
    public class GetMovieResponse : ResponseBase
    {
        public Movie Movie { get; set; }
    }
}
