﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Messaging.Admin
{
    public class GetAddMovieRequest
    {
        public Movie Movie { get; set; }
    }
}
