﻿using DomainModel;

namespace Services.Messaging.Admin
{
    public class GetEditMovieRequest
    {
        public Movie Movie { get; set; }
    }
}
