﻿namespace Services.Messaging.Admin
{
    public class GetDeleteMovieRequest
    {
        public int MovieId { get; set; }
    }
}
