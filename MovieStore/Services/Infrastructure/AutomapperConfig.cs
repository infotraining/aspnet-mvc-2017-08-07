﻿using DomainModel;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Services.Infrastructure
{
    public class AutomapperConfig
    {
        public static void ConfigureMappings()
        {
            Mapper.CreateMap<CartLine, CartLineViewModel>()
                .ForMember(dest => dest.MovieId, src => src.MapFrom(opt => opt.Movie.MovieId))
                .ForMember(dest => dest.MoviePrice, src => src.MapFrom(opt => opt.Movie.Price))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(
                    s => string.Format(
                        new CultureInfo("pl-PL"), "{0:c}", s.Movie.Price * s.Quantity
                    )))
                .ForMember(dest => dest.MovieTitle, src => src.MapFrom(opt => opt.Movie.Title));

            Mapper.CreateMap<Cart, CartViewModel>()
                .ForMember(dest => dest.TotalItems, src => src.MapFrom(opt => opt.CartSummary.TotalItems))
                .ForMember(dest => dest.Lines, src => src.MapFrom(opt => opt.Lines))
                .ForMember(dest => dest.TotalPrice, src => src.ResolveUsing(
                    s => string.Format(
                        new CultureInfo("pl-PL"), "{0:c}", s.CartSummary.TotalPrice))); 
        }
    }
}
