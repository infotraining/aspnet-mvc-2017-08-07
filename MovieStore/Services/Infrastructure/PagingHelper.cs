﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Infrastructure
{
    public class PagingHelper
    {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int CategoryId { get; set; }

        public int TotalItems { get; set; }
        public int TotalPages { get
            {
                return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
            }
        }
    }
}
