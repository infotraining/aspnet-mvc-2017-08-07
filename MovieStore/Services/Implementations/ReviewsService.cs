﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using DomainModel;
using DomainModel.Repositories;
using System.Linq;
using Services.ViewModels;
using Services.Messaging;

namespace Services.Implementations
{
    public class ReviewsService : IReviewsService
    {
        private readonly IReviewsRepository _reviewsRepository;

        public ReviewsService(IReviewsRepository reviewsRepository)
        {
            _reviewsRepository = reviewsRepository;
        }

        public GetAddReviewResponse AddReview(GetAddReviewRequest request)
        {
            var response = new GetAddReviewResponse();

            try
            {
                Review review = new Review
                {
                    Author = request.Review.Author,
                    Content = request.Review.Content,
                    DateAdded = request.Review.DateAdded,
                    MovieId = request.Review.MovieId,
                    Rating = request.Review.Rating
                };

                _reviewsRepository.Insert(review);
                _reviewsRepository.SaveChanges();

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public IEnumerable<ReviewViewModel> GetReviews()
        {
            var all = _reviewsRepository.GetAll().ToList();

            IEnumerable<ReviewViewModel> reviews = all.Select(x => new ReviewViewModel
            {
                Author = x.Author,
                DateAdded = x.DateAdded,
                Content = x.Content,
                Rating = x.Rating,
                MovieId = x.MovieId,
                MovieTitle = x.Movie.Title
            });

            return reviews;
        }
    }
}
