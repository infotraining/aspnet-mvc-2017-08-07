﻿using Services.Interfaces;
using System;
using DomainModel;
using Services.ViewModels;
using AutoMapper;

namespace Services.Implementations
{
    public class CartService : ICartService
    {
        public CartViewModel CreateCart(Cart cart)
        {
            return Mapper.Map<CartViewModel>(cart);
        }
    }
}
