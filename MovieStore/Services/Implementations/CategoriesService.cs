﻿using System;
using System.Collections.Generic;
using DomainModel;
using DomainModel.Repositories;
using Services.Interfaces;

namespace Services.Implementations
{
    public class CategoriesService : ICategoriesService
    {
        private readonly ICategoriesRepository _repo;

        public CategoriesService(ICategoriesRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<Category> GetCategories()
        {
            return _repo.GetAllIncluding(x => x.Movies);
        }

        public Category GetCategory(int id)
        {
            return _repo.GetById(id);
        }
    }
}
