﻿using Services.Interfaces;
using System;
using Services.Messaging.Admin;
using DomainModel.Repositories;

namespace Services.Implementations
{
    public class MoviesAdminService : IMoviesAdminService
    {
        private readonly IMoviesRepository _moviesRepository;

        public MoviesAdminService(IMoviesRepository moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }

        public GetAddMovieResponse AddMovie(GetAddMovieRequest request)
        {
            var response = new GetAddMovieResponse();

            try
            {
                _moviesRepository.Insert(request.Movie);
                _moviesRepository.SaveChanges();

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public GetDeleteMovieResponse DeleteMovie(GetDeleteMovieRequest request)
        {
            var response = new GetDeleteMovieResponse();

            try
            {
                _moviesRepository.Delete(request.MovieId);
                _moviesRepository.SaveChanges();

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public GetEditMovieResponse EditMovie(GetEditMovieRequest request)
        {
            var response = new GetEditMovieResponse();

            try
            {
                _moviesRepository.Update(request.Movie);
                _moviesRepository.SaveChanges();

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public GetMovieResponse GetMovie(GetMovieRequest request)
        {
            var response = new GetMovieResponse();

            try
            {
                response.Movie = _moviesRepository.GetById(request.MovieId);
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
