﻿using Services.Interfaces;
using DomainModel;
using DomainModel.Repositories;
using System.Linq;
using Services.Infrastructure;
using Services.ViewModels;

namespace Services.Implementations
{
    public class MoviesService : IMoviesService
    {
        private readonly IMoviesRepository _moviesRepository;

        public MoviesService(IMoviesRepository moviesReposirory)
        {
            _moviesRepository = moviesReposirory;
        }

        public Movie GetMovie(int movieId)
        {
            return _moviesRepository.GetById(movieId);
        }

        public MoviesListViewModel GetMovies(PagingHelper pagingHelper)
        {
            var response = new MoviesListViewModel();

            var movies = _moviesRepository.GetAll();

            if (pagingHelper.CategoryId > 0)
                movies = movies.Where(m => m.CategoryId == pagingHelper.CategoryId);

            int totalItems = movies.Count();

            if (pagingHelper.CurrentPage > 0)
                movies = movies
                    .OrderBy(m => m.MovieId)
                    .Skip((pagingHelper.CurrentPage - 1) * pagingHelper.ItemsPerPage)
                    .Take(pagingHelper.ItemsPerPage);

            response.PagingHelper = new PagingHelper
            {
                CategoryId = pagingHelper.CategoryId,
                CurrentPage = pagingHelper.CurrentPage,
                ItemsPerPage = pagingHelper.ItemsPerPage,
                TotalItems = totalItems
            };

            response.Movies = movies;

            return response;
        }

        //public IEnumerable<Movie> GetMovies(int categoryId)
        //{
        //    if (categoryId == 0)
        //        return _moviesRepository.GetAll();

        //    IQueryable<Movie> movies = _moviesRepository.GetAll().Where(m => m.CategoryId == categoryId);

        //    return movies;
        //}
    }
}
