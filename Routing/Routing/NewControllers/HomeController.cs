﻿using System.Web.Mvc;

namespace Routing.NewControllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [Route("Test")]
        public ActionResult Index()
        {
            ViewBag.Controller = "HomeController from NewControllers";
            ViewBag.Action = "Index";
            return View("Result");
        }
    }
}