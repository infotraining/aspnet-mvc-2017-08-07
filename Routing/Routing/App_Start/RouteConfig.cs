﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Routing
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();
            
            routes.MapRoute(name: "", url: "domowa/metoda-{action}", defaults: new { controller = "Home" });

            routes.MapRoute(
               name: "MyRoute",
               url: "Home/Index",
               defaults: new { controller = "Home", action = "Index" });

            routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });


           //routes.MapRoute(
           //     name: "Default", 
           //     url: "{controller}/{action}/{id}", 
           //     defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
           //     constraints: new
           //     {
           //         controller = "^H.*",
           //         action = "^Index$|^List$",
           //         httpMethod = new HttpMethodConstraint("GET"),
           //         id = new CompoundRouteConstraint(new IRouteConstraint[] {
           //             new AlphaRouteConstraint(), new MinLengthRouteConstraint(6) }),
           //         customConstraint = new UserAgentConstraint("Chrome")
           //     },
           //     namespaces: new[] { "Routing.Controllers" });

            ////przy zmianie kontrolera stała ścieżka
            //routes.MapRoute(
            //     name: "Client",
            //     url: "Client/{action}",
            //     defaults: new { controller = "Client"}
            // );

            ////kolejność ścieżek
            //routes.MapRoute(
            //     name: "",
            //     url: "Public{controller}/{action}"
            // );
            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}",
            //    defaults: new {action = "Index"}
            //);
        }
    }
}
