﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Routing.Infrastructure
{
    public class UserAgentConstraint : IRouteConstraint
    {
        private readonly string _browser;

        public UserAgentConstraint(string browser)
        {
            _browser = browser;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            bool isMatch = httpContext.Request.UserAgent != null && httpContext.Request.UserAgent.Contains(_browser);

            return isMatch;
        }
    }
}