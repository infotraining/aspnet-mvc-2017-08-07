﻿using System.Web.Mvc;

namespace Routing.Controllers
{
    //[RoutePrefix("domowy")]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "Index";
            return View("Result");
        }
        

        public ActionResult List()
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "List";
            return View("Result");
        }

        public ActionResult WithId(string id = "23")
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "WithID";
            //ViewBag.Id = RouteData.Values["id"];
            //ViewBag.Id = Request.QueryString["id"];
            ViewBag.Id = id;
            ViewBag.Catchall = RouteData.Values["catchall"];

            return View("Result");
        }
    }
}