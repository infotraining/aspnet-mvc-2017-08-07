﻿using System.Web.Mvc;

namespace Routing.Controllers
{
    public class ClientController : Controller
    {
        // GET: Client
        public ActionResult Index()
        {
            ViewBag.Controller = "Client";
            ViewBag.Action = "Index";
            return View("Result");
        }

        [Route("Test")]
        public ActionResult List()
        {
            ViewBag.Controller = "Client";
            ViewBag.Action = "List";
            //return RedirectToAction("Index", "Home");
            return RedirectToRoute("MyRoute");
        }
    }
}