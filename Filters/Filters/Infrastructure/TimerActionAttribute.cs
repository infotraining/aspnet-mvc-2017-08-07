﻿using System;
using System.Diagnostics;
using System.Web.Mvc;

namespace Filters.Infrastructure
{
    public class TimerActionAttribute : FilterAttribute, IActionFilter
    {
        private Stopwatch timer;

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            timer.Stop();

            if (filterContext.Exception == null)
            {
                filterContext.HttpContext.Response.Write(
                    string.Format("<div>Akcja się wykonywała przez {0} ms </div>", timer.Elapsed.TotalMilliseconds));
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            timer = Stopwatch.StartNew();
        }
    }
}