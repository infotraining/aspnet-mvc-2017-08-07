﻿using System;
using System.Web.Mvc;

namespace Filters.Infrastructure
{
    public class CustomRangeExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled == false && 
                filterContext.Exception is ArgumentOutOfRangeException)
            {
                //filterContext.Result = new RedirectResult("~/Content/RangeErrorPage.html");

                int value = (int)((ArgumentOutOfRangeException)filterContext.Exception).ActualValue;

                filterContext.Result = new ViewResult
                {
                    ViewName = "RangeError",
                    ViewData = new ViewDataDictionary<int>(value)
                };

                filterContext.ExceptionHandled = true;
            }
        }
    }
}