﻿using System.Web;
using System.Web.Mvc;

namespace Filters.Infrastructure
{
    public class LocalRequestsAllowedAttribute : AuthorizeAttribute
    {
        private bool isAllowed;

        public LocalRequestsAllowedAttribute(bool allowed)
        {
            isAllowed = allowed;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.IsLocal)
            {
                return isAllowed;
            }
            else
            {
                return true;
            }
        }
    }
}