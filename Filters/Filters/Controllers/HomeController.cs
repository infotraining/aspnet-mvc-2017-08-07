﻿using Filters.Infrastructure;
using System.Web.Mvc;
using System.Web.Security;
using System;

namespace Filters.Controllers
{
    //[TimerAll]
    [SimpleMessage(Message = "Trzeci")]
    public class HomeController : Controller
    {
        //[LocalRequestsAllowed(false, Users = "admin")]
        //[ArvatoAuthentication]
        //[Authorize(Users = "mateusz@arvato.com")]
        //[TimerAction]
        //[TimerResult]
        //[TimerAll]

        [SimpleMessage(Message = "Pierwszy", Order = 1)]
        [SimpleMessage(Message = "Drugi", Order = 1)]
        [SimpleMessage(Message = "Trzeci")]
        public ActionResult Index()
        {
            ViewBag.UserName = User.Identity.Name;
            return View();
        }

        ////[CustomRangeException]
        //[HandleError(ExceptionType= typeof(ArgumentOutOfRangeException), View = "RangeError")]
        //[CustomOverride]
        public string RangeInput(int number)
        {
            if (number > 10 && number < 20)
            {
                return string.Format("Podany numer to {0}", number);
            }
            else
            {
                throw new ArgumentOutOfRangeException("number", number, "Ma być między 10 a 20");
            }
        }
    }
}