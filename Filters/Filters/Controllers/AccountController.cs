﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Filters.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string name, string password, string returnUrl)
        {
            bool result = FormsAuthentication.Authenticate(name, password);

            if (result)
            {
                FormsAuthentication.SetAuthCookie(name, false);
                return Redirect(returnUrl ?? Url.Action("Index", "Home"));
            }
            else
            {
                ModelState.AddModelError("", "Niepoprawne dane logowania");
                return View();
            }
        }
    }
}