﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestsDemo.Controllers;
using System.Web.Mvc;

namespace UnitTestsDemo.Tests
{
    [TestClass]
    public class FizzBuzzControllerTest
    {
        [TestMethod]
        public void For1Returns1()
        {
            //arrange
            FizzBuzzController controller = new FizzBuzzController();

            //act
            ViewResult result = controller.Index(1) as ViewResult;

            //assert
            Assert.AreEqual("1", result.ViewBag.Output);
        }

        [TestMethod]
        public void For3Returns12Fizz()
        {
            //arrange
            FizzBuzzController controller = new FizzBuzzController();

            //act
            ViewResult result = controller.Index(3) as ViewResult;

            //assert
            Assert.AreEqual("1 2 Fizz", result.ViewBag.Output);

        }

        [TestMethod]
        public void For5Returns12Fizz4Buzz()
        {
            //arrange
            FizzBuzzController controller = new FizzBuzzController();

            //act
            ViewResult result = controller.Index(5) as ViewResult;

            //assert
            Assert.AreEqual("1 2 Fizz 4 Buzz", result.ViewBag.Output);

        }

        [TestMethod]
        public void For15ReturnsCorrectOutput()
        {
            //arrange
            FizzBuzzController controller = new FizzBuzzController();

            //act
            ViewResult result = controller.Index(15) as ViewResult;

            //assert
            Assert.AreEqual("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz", result.ViewBag.Output);

        }
    }
}
