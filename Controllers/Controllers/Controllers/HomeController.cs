﻿using System;
using System.Web.Mvc;

namespace Controllers.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.UserName = User.Identity.Name;
            ViewBag.ServerName = Server.MachineName;
            ViewBag.ClientIp = Request.UserHostAddress;
            ViewBag.TimeStamp = HttpContext.Timestamp;

            TempData["TimeStamp"] = "Hello World";

            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            ViewBag.TimeStamp = (string)TempData["TimeStamp"];

            return RedirectToAction("List2");
        }

        public ActionResult List2()
        {
            ViewBag.TimeStamp = TempData["TimeStamp"];

            return View("Index");
        }

        public string ShowName(string name, DateTime? createdTime, int id = 1)
        {
            //przeszukuje: Request.QueryString, Request.From, RouteData.Values, Request.Files
            return name + createdTime;
        }
    }
}